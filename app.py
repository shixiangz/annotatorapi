#!flask/bin/python

from flask import Flask, jsonify, Response, request, render_template
import json
import os
import subprocess
import HTMLParser
from werkzeug.utils import secure_filename

app = Flask(__name__)

tasks = [
	{
		'id': 1,
		'title': u'Buy groceries',
		'description': u'Milk, Cheese, Pizza, Fruit, Tylenol', 
		'done': False
	},
	{
		'id': 2,
		'title': u'Learn Python',
		'description': u'Need to find a good Python tutorial on the web', 
		'done': False
	}
]

annotation = {
	"text": "Cancer-selective targeting of the NF-XB survival pathway with GADD45A/MKK7 inhibitors.",
	"denotations":[
		{"span":{"begin":34,"end":39},"obj":"4790"},
		{"span":{"begin":62,"end":69},"obj":"4616"},
		{"span":{"begin":70,"end":74},"obj":"5609"}
	]
}

jsonFileDir = "annotation.json"

# dictList = []

app.config['UPLOAD_FOLDER'] = "./dicts/"

@app.route('/')
def index():
	resp = Response("Hello, World!")
	resp.headers['Access-Control-Allow-Origin'] = '*'
	return resp

@app.route('/getAnnotation', methods=['GET'])
def getAnnotation():
	para = request.args
	# if para.get('text') == '':
	# 	error = jsonify({"error":"empty input"})
	# 	error.headers['Access-Control-Allow-Origin'] = '*'
	# 	return error, 500
	annotationJson = _execCmd(para)
	rows = _generateTable(annotationJson)
	parser = HTMLParser.HTMLParser()
	html = parser.unescape(render_template('main.html', annotationJson = json.dumps(annotationJson), rows = rows))
	resp = Response(html)
	resp.headers['Access-Control-Allow-Origin'] = '*'
	return resp

@app.route('/getJson', methods=['GET'])
def getJson():
	annotation = json.load(open('annotation.json', 'r'))
	resp = jsonify(annotation)
	resp.headers['Access-Control-Allow-Origin'] = '*'
	return resp

@app.route('/deleteJsonFile', methods=['POST'])
def deleteJsonFile():
	_deleteJsonFile()
	resp = jsonify({ "result": "success" })
	resp.headers['Access-Control-Allow-Origin'] = '*'
	return resp

@app.route('/finishQuery', methods=['POST'])
def finishiQuery():
	data = { "text": "undefined", "denotations":[] }
	_writeJsonFile(data)
	resp = jsonify({ "result": "success" })
	resp.headers['Access-Control-Allow-Origin'] = '*'
	return resp

@app.route('/dictionaries', methods=['GET'])
def getDictionaries():
	dictList = getDictList()
	resp = jsonify({ "dictList": dictList })
	resp.headers['Access-Control-Allow-Origin'] = '*'
	return resp

@app.route('/uploadajax', methods = ['POST'])
def upldfile():
	if request.method == 'POST':
		file = request.files['file']
		filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        cmd = 'scala -classpath read_annotation.jar main.wrapper.CTDAnnotation insert dicts/' + filename + ' OBO'
        print(cmd)
        subprocess.call(cmd, shell = True)
        getDictList()
	resp = jsonify({ "result": "success" })
	resp.headers['Access-Control-Allow-Origin'] = '*'
	return resp

# private methods
def _execCmd(para):
	text = para.get('text')
	findAllMatched = para.get('findAllMatched')
	caseSensitive = para.get('caseSensitive')
	orderIndependentLookup = para.get('orderIndependentLookup')
	searchStrategy = para.get('searchStrategy')
	dictionaries = para.getlist('dictionaries[]')
	print(text, findAllMatched, caseSensitive, orderIndependentLookup, searchStrategy, dictionaries)
	
	para = ''
	para += '-f ' if findAllMatched == 'true' else ''
	para += '-c ' if caseSensitive == 'true' else ''
	para += '-o ' if orderIndependentLookup == 'true' else ''
	para += '-s ' if searchStrategy == 'Skip Any Match' else ''
	para += '"' + text + '"'

	resultStrList = []
	for dictionary in dictionaries:
		cmd = 'scala -classpath read_annotation.jar main.wrapper.CTDAnnotation getanno '
		cmd += dictionary + ' '
		cmd += para

		resultStr = ''
		p = subprocess.Popen(cmd, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
		for line in iter(p.stdout.readline, b''):
			resultStr = line.rstrip()
		resultStrList.append(resultStr)

	return _parseResult(text, resultStrList)

def _parseResult(text, resultStrList):
	resultJson = {}
	denotations = []
	resultJson['text'] = text
	for resultStr in resultStrList:
		for line in json.loads(resultStr):
			for term in line:
				denotations.append({"span":{"begin":term['start_ofset'],"end":term['end_offset']},"obj":term['dict_type']})
	resultJson['denotations'] = denotations

	print(resultJson)
	return resultJson

def _writeJsonFile(data):
	# dir = os.path.dirname(jsonFileDir)
	# if not os.path.exists(dir):
	#     os.makedirs(dir)
	with open(jsonFileDir, 'w') as f:
		json.dump(data, f)

def _deleteJsonFile():
	if os.path.isfile(jsonFileDir):
		os.remove(jsonFileDir)

def _generateTable(json):
	text = json['text']
	denotations = json['denotations']
	rows = []
	for denotation in denotations:
		span = denotation['span']
		term = text[span['begin']: span['end']]
		obj = denotation['obj']
		tableRowData = {
			'term': term,
			'span': str(span['begin']) + '-' + str(span['end']),
			'obj': obj
		}
		rows.append(tableRowData)
	return rows

def initEngine():
	print('Initializing annotation engine...')
	subprocess.call('scala -classpath read_annotation.jar main.wrapper.CTDAnnotation insert dicts/a.xml Type', shell = True)
	subprocess.call('scala -classpath read_annotation.jar main.wrapper.CTDAnnotation insert dicts/b.tsv disease', shell = True)
	subprocess.call('scala -classpath read_annotation.jar main.wrapper.CTDAnnotation insert dicts/c.obo OBO', shell = True)
	getDictList()

def getDictList():
	dictList = []
	cmd = 'scala -classpath read_annotation.jar main.wrapper.CTDAnnotation getdictlist'
	p = subprocess.Popen(cmd, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
	for line in iter(p.stdout.readline, b''):
		resultStr = line.rstrip()
		for dictName in json.loads(resultStr):
			dictList.append(dictName['dict_name'])
	print('dictionaries list: ', dictList)
	return dictList

if __name__ == '__main__':
	initEngine()
	app.debug=True
	# app.run(port=8000)
	app.run(host = '192.168.56.101', port = 8000)
